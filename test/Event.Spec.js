import {Create, Update, Get, GetAll, Delete} from '../lib/event/Event'
import {expect} from 'chai'
describe('Funciones Event', () => {

  let fecha = new Date('11/01/2016')

  describe('> Create', () => {
    it('Deberia Crear Un Nuevo evento y responder true', (done) => {
      expect(Create({nombre: 'prueba',descripcion: '' , fecha: fecha.getTime()}))
      .to.eventually.be.equal(true).notify(done)
    })
  })
  describe('> Update', () => {
    it('Deberia Editar Un Nuevo evento y responder true', (done) => {
      expect(Update({nombre: 'prueba',descripcion: 'Nueva descripcion' , fecha: fecha.getTime()}))
      .to.eventually.be.equal(true).notify(done)
    })
  })
  describe('> Get', () => {
    it('Deberia buscar un evento y regresar un objeto', (done) => {
      expect(Get({nombre: 'prueba'}))
      .to.eventually.be.an('object').notify(done)
    })
  })
  describe('> GetAll', () => {
    it('Deberia buscar todos los eventos y regresar un arreglo', (done) => {
      expect(GetAll())
      .to.eventually.be.an('array').notify(done)
    })
  })
  describe('> Delete', () => {
    it('Deberia borrar un evento y responder true', (done) => {
      expect(Delete({nombre: 'prueba'}))
      .to.eventually.be.equal(true).notify(done)
    })
  })
})
