import {SignUp, SignDown, SignIn} from '../lib/event/Sign'
import {expect} from 'chai'

describe('Funciones Sign', () => {
  describe('> SignUp', () => {
    it('Deberia crear un nuevo usuario y responder 1', (done) => {
      expect(SignUp({user: 'prueba', password: 123456}))
      .to.eventually.be.equal(true).notify(done)
    })
  })
  describe('> SignIn', () => {
    it('Deberia buscar este usuario, y regresar true si le información es correcta', (done) => {
      expect(SignIn({user: 'prueba', password: 123456}))
      .to.eventually.be.equal(true).notify(done)
    })
  })
  describe('> SignDown', () => {
    it('Deberia eliminar al usuario prueba, y responder 1 si todo salio bien', (done) => {
      expect(SignDown({user: 'prueba'}))
      .to.eventually.be.equal(true).notify(done)
    })
    it('Deberia eliminar al usuario prueba2 y responder 0 por que el usuario no existe', (done) => {
      expect(SignDown({user: 'prueba2'}))
      .to.eventually.be.equal(false).notify(done)
    })
  })
})
