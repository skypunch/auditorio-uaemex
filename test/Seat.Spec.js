import {SetState, GetState, Resume, Get} from '../lib/event/Seat'
import {expect} from 'chai'

describe('Funciones Seat', () => {
  SetState('prueba', 100, 1)
  describe('> SetState', () => {
    it('Deberia Poner el Estado del asiento 1 en 01', (done) => {
      expect(SetState('prueba', 1, 1))
      .to.eventually.be.equal('01').notify(done)
    })
    it('Deberia Poner el Estado del asiento 1 en 10', (done) => {
      expect(SetState('prueba', 1, 1))
      .to.eventually.be.equal('10').notify(done)
    })
    it('Deberia Poner el Estado del asiento 1 en 10', (done) => {
      expect(SetState('prueba', 1, 1))
      .to.eventually.be.equal('10').notify(done)
    })
    it('Deberia Poner el Estado del asiento 1 en 00', (done) => {
      expect(SetState('prueba', 1, 0))
      .to.eventually.be.equal('00').notify(done)
    })
    it('Deberia Poner el Estado del asiento 2 en 00', (done) => {
      expect(SetState('prueba', 2, 0))
      .to.eventually.be.equal('00').notify(done)
    })
    it('Deberia Poner el Estado del asiento 2 en 01', (done) => {
      expect(SetState('prueba', 2, 1))
      .to.eventually.be.equal('01').notify(done)
    })
    it('Deberia Poner el Estado del asiento 2 en 00', (done) => {
      expect(SetState('prueba', 2, 0))
      .to.eventually.be.equal('00').notify(done)
    })
  })
  describe('> GetState', () => {
    it('Deberia de regresar 01 para el asiento 100', (done) => {
      expect(GetState('prueba', 100))
      .to.eventually.be.equal('01').notify(done)
    })
    it('Deberia de regresar 00 para el asiento 1', (done) => {
      expect(GetState('prueba', 1))
      .to.eventually.be.equal('00').notify(done)
    })
  })
  describe('> Resume', () => {
    it('Deberia regresar un objeto', (done) => {
      expect(Resume('prueba'))
      .to.eventually.be.an('object').notify(done)
    })
  })
  describe('> Get', () => {
    it('Deberia regresar un arreglo', (done) => {
      expect(Get('prueba'))
      .to.eventually.be.an('array').notify(done)
    })
  })
})

setTimeout(function () {
  SetState('prueba', 100, 0)
}, 2000)
