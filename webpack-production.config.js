const webpack = require('webpack')
const path = require('path')
const buildPath = path.resolve(__dirname, 'public')
const nodeModulesPath = path.resolve(__dirname, 'node_modules')
const projectPath = [path.resolve(__dirname, 'lib'), path.resolve(__dirname, 'guia'), path.resolve(__dirname, 'helpers'), path.resolve(__dirname, 'test')]
const TransferWebpackPlugin = require('transfer-webpack-plugin')

const config = {
  entry: [path.join(__dirname, '/src/app/app.js')],
  devtool: 'source-map',
  output: {
    path: buildPath,
    filename: 'app.js'
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      }
    }),
    new webpack.NoErrorsPlugin(),
    new TransferWebpackPlugin([
      {from: 'www'}
    ], path.resolve(__dirname, 'src'))
  ],
  module: {
    loaders: [
      {
        test: /\.js$/,
        loaders: ['babel-loader'],
        exclude: [nodeModulesPath, ...projectPath]
      }
    ]
  }
}
module.exports = config
