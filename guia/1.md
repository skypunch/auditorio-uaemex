# Estructura del Proyecto

* **/lib**
  + ***/event***
* **/helpers**
* **/src**
* **/public**
* **server.js**

## lib

La carpeta ***"lib"*** contiene todas las funciones propias de la aplicación

la carpeta ***"event"*** contiene todos los eventos que se utilizaran para los sockets

## helpers

la carpeta ***"helpers"*** contiene todas las funciones que son auxiliares para el funcionamiento de la aplicación

## src

contiene lo necesario para el desarrollo del cliente (vistas)

## public

contendra el cliente ya compilado

## server.js

El archivo ***server.js*** es el entry point de la aplicación
