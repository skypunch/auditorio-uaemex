import crypto from 'crypto'

export default (length = 16, type = 'hex') => crypto.randomBytes(length).toString(type)
