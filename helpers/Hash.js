import crypto from 'crypto'

export default (alg = 'sha256', type = 'hex') => {
  const hash = crypto.createHash(alg)
  return (data) => {
    let dig = ''
    hash.on('readable', () => {
      let data = hash.read()
      if (data) {
        dig = data.toString(type)
      }
    })
    hash.write(data)
    hash.end()
    return dig
  }
}
