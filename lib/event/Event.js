import redis from 'redis'
const client = redis.createClient()

export const Create = (data) => {
  return new Promise(function (resolve, reject) {
    client.setnx(`event:${data.nombre}`, JSON.stringify(data), (err, reply) => {
      if (err) reject(err)
      else {
        client.PEXPIREAT(`event:${data.nombre}`, data.fecha)
        resolve(reply === 1)
      }
    })
  })
}

export const Update = (data) => {
  return new Promise(function (resolve, reject) {
    client.set(`event:${data.nombre}`, JSON.stringify(data), (err, reply) => {
      if (err) reject(err)
      else {
        client.PEXPIREAT(`event:${data.nombre}`, data.fecha)
        resolve(reply === 'OK')
      }
    })
  })
}

export const Get = (data) => {
  return new Promise(function (resolve, reject) {
    client.get(`event:${data.nombre}`, (err, reply) => {
      if (err) reject(err)
      else {
        resolve(JSON.parse(reply))
      }
    })
  })
}

export const GetAll = (data) => {
  return new Promise(function (resolve, reject) {
    client.keys('event:*', (error, keys) => {
      if (error) reject(error)
      client.mget(keys, (err, reply) => {
        if (err) reject(err)
        else {
          resolve(reply.map((element) => JSON.parse(element)))
        }
      })
    })
  })
}

export const Delete = (data) => {
  return new Promise(function (resolve, reject) {
    client.del(`event:${data.nombre}`, (err, reply) => {
      if (err) reject(err)
      resolve(reply === 1)
    })
  })
}
