import redis from 'redis'
const client = redis.createClient()

export const SignUp = (data) => {
  return new Promise(function (resolve, reject) {
    client.setnx(`user:${data.user}`, data.password, (err, reply) => {
      if (err) reject(err)
      resolve(reply === 1)
    })
  })
}

export const SignDown = (data) => {
  return new Promise(function (resolve, reject) {
    client.del(`user:${data.user}`, (err, reply) => {
      if (err) reject(err)
      resolve(reply === 1)
    })
  })
}

export const SignIn = (data) => {
  return new Promise(function (resolve, reject) {
    client.get(`user:${data.user}`, (err, reply) => {
      if (err) reject(err)
      else {
        resolve(reply === data.password.toString())
      }
    })
  })
}
