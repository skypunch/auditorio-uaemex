import Bitwise from 'simple-bitwise'
import redis from 'redis'
const client = redis.createClient()
const bufferClient = redis.createClient({return_buffers: true})

const byte = new Bitwise()

export const SetState = (event, index, state) => {
  index--
  return new Promise(function (resolve, reject) {
    client.getbit(`seats:${event}:active`, index, function (err, active) {
      if (err) reject(err)
      client.getbit(`seats:${event}:tmp`, index, function (er, tmp) {
        if (er) reject(er)
        if (state === 1) {
          if (active === 0 && tmp === 0) {
            client.setbit(`seats:${event}:tmp`, index, 1)
            resolve('01')
          } else if (active === 0 && tmp === 1) {
            client.setbit(`seats:${event}:tmp`, index, 0)
            client.setbit(`seats:${event}:active`, index, 1)
            resolve('10')
          } else {
            resolve('10')
          }
        } else {
          client.setbit(`seats:${event}:tmp`, index, 0)
          client.setbit(`seats:${event}:active`, index, 0)
          resolve('00')
        }
      })
    })
  })
}

export const GetState = (event, index) => {
  index--
  return new Promise(function (resolve, reject) {
    client.getbit(`seats:${event}:active`, index, function (err, active) {
      if (err) reject(err)
      client.getbit(`seats:${event}:tmp`, index, function (er, tmp) {
        if (er) reject(er)
        resolve(`${active}${tmp}`)
      })
    })
  })
}

export const Resume = (event) => {
  return new Promise(function (resolve, reject) {
    client.bitcount(`seats:${event}:active`, function (err, active) {
      if (err) reject(err)
      client.bitcount(`seats:${event}:tmp`, function (er, tmp) {
        if (er) reject(er)
        resolve({
          reserved: tmp,
          actives: active
        })
      })
    })
  })
}

export const Get = (event) => {
  return new Promise(function (resolve, reject) {
    bufferClient.get(`seats:${event}:active`, function (err, active) {
      if (err) reject(err)
      bufferClient.get(`seats:${event}:tmp`, function (er, tmp) {
        if (er) reject(er)
        let dataTmp = tmp.toJSON().data.map((current) => byte.toBitString(current))
        let dataActive = active.toJSON().data.map((current) => byte.toBitString(current))
        let result = []
        for (let i = 0; i < dataTmp.length; i++) {
          for (let j = 0; j < 8; j++) {
            result = [...result, `${dataActive[i][j]}${dataTmp[i][j]}`]
          }
        }
        resolve(result)
      })
    })
  })
}
